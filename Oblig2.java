/* 
Solution of oblig 2 in inf3110 H2015, by Finn-Haakon Tuft 
Writing a interpreter for the Robol language in java
*/
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.ArrayList;

enum Direction {
    NORTH,SOUTH,WEST,EAST
}

interface Robol {
    void interpret();
}

final class Start {
    private final Expression xExp,yExp;
    private final Grid grid;
    public Start(final Expression xExp,final Expression yExp,final Grid grid) {
        this.xExp = xExp; this.yExp = yExp; this.grid = grid;
    }
    void startProgram() {
        this.grid.setPos(this.xExp.evaluate(),this.yExp.evaluate());
    }
}

final class Grid {
    private final int x,y;//size of grid
    private int xPos,yPos;

    public Grid(final int x,final int y) {
        this.x = x; this.y = y;
    }

    void setPos(final int xPos,final int yPos) {
        this.xPos = xPos; this.yPos = yPos;
    }

    void addPos(final int x,final int y) {
        this.xPos += x; this.yPos += y;
        if(abs(this.xPos) > this.x || abs(this.yPos) > this.y) {
            System.out.print("Outside of grid: ");
            this.printGrid();
            System.exit(0);
         }
    }
    
    void printGrid() {
        System.out.println("x pos:" + this.x + " y pos:"+ this.y + " of robot");    
    }   
}

class Program implements Robol {
    private final Grid grid;
    private final Robot robot;

    public Program(final Grid grid, final Robot robot) {
        this.grid = grid; this.robot = robot;
    }
    
    public void interpret() {//Only interpert seen outsid of packet
        robot.interpret(); 
    }
}

class Robot implements Robol {

    private final HashMap<String,Integer> varDec;
    private final Start start;
    private final ArrayList<Statment> list;

    public Robot(final HashMap<String,Integer> varDec,final Start start,final ArrayList<Statment> list) {
        this.varDec = varDec; this.start = start; this.list = list;
    }

    public void interpret() {
        this.start.startProgram();
        for(final Statment stm: this.list) {
           stm.interpret();
        }
    }
}

abstract class Statment implements Robol {
    abstract public void interpret();
}

final class Stop extends Statment {
    private final Grid grid;
    public Stop(final Grid grid) {
        this.grid = grid;
    }

    public void interpret() {
        this.grid.printGrid();
        System.exit(0);
    }
} 

final class Assignment extends Statment {
    private final HashMap<String,Integer> vars;
    private final Expression exp;
    private final String key;    

    public Assignment(final HashMap<String,Integer> vars,final String key,Expression exp) {
        this.vars = vars; this.key = key;this.exp = exp; 
    }

    public void interpret() {
    
        final Integer num = vars.remove(this.key);
        assert(num != null): "Sorry my bad:(";
        vars.put(this.key,new Integer(this.exp.evaluate()));//TODO: Can this ever go wronge?
    }
}

final  class Move extends Statment {
    private final Grid grid;
    private final Direction direction;
    private final Expression exp; 
    
    public Move(final Direction direction,final Expression exp,final Grid grid) {
        this.direction = direction; this.exp = exp; this.grid = grid;
    }
    
    public void interpret() {
        switch(direction) {
            case NORTH:
                this.grid.addPos(0,exp.evaluate());
                break;
            case SOUTH:
                this.grid.addPos(0,-exp.evaluate());
                break;
            case WEST:  
                this.grid.addPos(exp.evaluate(),0);
                break;
            case EAST:  
                this.grid.addPos(-exp.evaluate(),0);
                break;
        }
    }
}

final class While extends Statment {
    private final BoolExp bexp;
    private final  ArrayList<Statment> list;

    public While(final BoolExp bexp,final ArrayList<Statment> list) {
        this.bexp = bexp; this.list = list;
        assert list != null: "stmList+"; //Must be min one in list
    }

    public void interpret() {
        while(bexp.evaluate() == 1) {
            for(final Statment stm : this.list) {
                stm.interpret();
            }
        }
    } 
}

abstract class Expression {
    abstract int evaluate();
}

final class Identifier extends Expression {
    private final String Key;
    private final HashMap<String,Integer> vars;
    public Identifier(final String Key,final HashMap<String,Integer> vars) {
        this.Key = Key; this.vars =vars;
    }   
    int evaluate() {
        return vars.get(Key).intValue();
    }
}

abstract class BoolExp extends Expression {
    final Expression left,right;
    public BoolExp(final Expression left,final Expression right) {
        this.left = left; this.right = right;
    }
}

final class Number extends Expression {
    private final int number;
    public Number(final int number) {
        this.number = number;
    }
    int evaluate() {
        return number;
    }
}

final class LargerThan extends BoolExp {
    public LargerThan(final Expression left, final Expression right) {
        super(left,right);
    }   
    int evaluate() {
        if(left.evaluate() > right.evaluate())
            return 1;
        else 
            return 0;      
    }
}

final class LessThan extends BoolExp {
    public LessThan(final Expression left, final Expression right) {
        super(left,right);
    }   
    int evaluate() {
        if(left.evaluate() < right.evaluate())
            return 1;
        else 
            return 0;      
    }
}

final class Equal extends BoolExp {
     public Equal(final Expression left, final Expression right) {
        super(left,right);
    }   
    int evaluate() {
        if(left.evaluate() == right.evaluate())
            return 1;
        else 
            return 0;      
    }
}

abstract class ArithExp extends Expression {
    final Expression left,right;
    public ArithExp(final Expression left,final Expression right) {
        this.left = left; this.right = right;        
    }
}

final class Pluss extends ArithExp {
    public Pluss(final Expression left, final Expression right) {
        super(left,right);
    }
    int evaluate() {
        return left.evaluate() + right.evaluate();
    }
}

final class Minus extends ArithExp {
    public Minus(final Expression left, final Expression right) {
        super(left,right);
    }

    int evaluate() {
        return left.evaluate() - right.evaluate();
    }
}

final class Mult extends ArithExp {
    public Mult(final Expression left, final Expression right) {
        super(left,right);
    }

    int evaluate() {
        return left.evaluate()*right.evaluate();
    }
}
